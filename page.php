<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gourmet Nuts & Dried Fruits
 */

get_header(); ?>

<main id="main" class="site-main">
	<div class="full-width blog-archive">

		<?php
		// if image is present display hero
		$image=  get_the_post_thumbnail_url();
		if ( $image ) {
			?>
			<div class="hero">
			<style>
			.page_hero::after {
				content: "";
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				background-size: cover;
				background-repeat: no-repeat;
				opacity: 0.75;
				background-image:url(<?php echo $image ?>);
				
				}
			</style>
				<header class="entry-header container">
					<?php the_title( '<h1 class="entry-title wrap">', '</h1>' ); ?>
				</header><!-- .entry-header -->
			</div>
		<?php
		}

		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
			
	</div>			
	</main><!-- #main -->

<?php get_footer(); ?>
