<?php
/**
 * Gourmet Nuts & Dried Fruits functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Gourmet Nuts & Dried Fruits
 */

if ( ! function_exists( 'hyd__setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * @author WDS
	 */
	function hyd__setup() {
		/**
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Gourmet Nuts & Dried Fruits, use a find and replace
		 * to change 'gns' to the name of your theme in all the template files.
		 * You will also need to update the Gulpfile with the new text domain
		 * and matching destination POT file.
		 */
		load_theme_textdomain( 'gns', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'full-width', 1920, 1080, false );
		add_image_size( 'small', 430, 259, true );

		/**
		 * Enable support sizing for Woocommerce gallery images.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'woocommerce', array(
			'thumbnail_image_width' => 200,
			'gallery_thumbnail_image_width' => 100,
			'single_image_width' => 350,
		) );

		// Register navigation menus.
		register_nav_menus(
			array(
				'primary' => esc_html__( 'Primary Menu', 'gns' ),
				'category-menu'  => esc_html__( 'Category Menu', 'gns' ),
				'mobile'  => esc_html__( 'Mobile Menu', 'gns' ),
				'footer'  => esc_html__( 'footer Menu', 'gns' ),
			)
		);

		/**
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'hyd__custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Custom logo support.
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 500,
				'flex-height' => true,
				'flex-width'  => true,
				'header-text' => array( 'site-title', 'site-description' ),
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Gutenberg color palette support.
		add_theme_support( 'editor-color-palette', hyd__get_theme_colors_gutenberg() );

		// Gutenberg support for full-width/wide alignment of supported blocks.
		add_theme_support( 'align-wide' );

		// Gutenberg defaults for font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name' => __( 'Small', 'gns' ),
					'size' => 12,
					'slug' => 'small',
				),
				array(
					'name' => __( 'Normal', 'gns' ),
					'size' => 16,
					'slug' => 'normal',
				),
				array(
					'name' => __( 'Large', 'gns' ),
					'size' => 36,
					'slug' => 'large',
				),
				array(
					'name' => __( 'Huge', 'gns' ),
					'size' => 50,
					'slug' => 'huge',
				),
			)
		);

		// Gutenberg editor styles support.
		add_theme_support( 'editor-styles' );
		add_editor_style( 'style-editor.css' );

		// Gutenberg responsive embed support.
		add_theme_support( 'responsive-embeds' );
	}
endif; add_action( 'after_setup_theme', 'hyd__setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 * @author WDS
 */
function hyd__content_width() {
	$GLOBALS['content_width'] = apply_filters( 'hyd__content_width', 640 );
} add_action( 'after_setup_theme', 'hyd__content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 * @author WDS
 */
function hyd__widgets_init() {

	// Define sidebars.
	$sidebars = array(
		'sidebar-1' => esc_html__( 'Sidebar 1', 'gns' )
	);

	// Loop through each sidebar and register.
	foreach ( $sidebars as $sidebar_id => $sidebar_name ) {
		register_sidebar(
			array(
				'name'          => $sidebar_name,
				'id'            => $sidebar_id,
				'description'   => /* translators: the sidebar name */ sprintf( esc_html__( 'Widget area for %s', 'gns' ), $sidebar_name ),
				'before_widget' => '<aside class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			)
		);
	}
} add_action( 'widgets_init', 'hyd__widgets_init' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load styles and scripts.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Load custom ACF features.
 */
require get_template_directory() . '/inc/acf.php';

/**
 * Load ACF Gutenberg block registration.
 */
require get_template_directory() . '/inc/acf-gutenberg.php';

/**
 * Load custom ACF search functionality.
 */
require get_template_directory() . '/inc/acf-search.php';

/**
 * Load custom filters and hooks.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Load custom queries.
 */
require get_template_directory() . '/inc/queries.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Scaffolding Library.
 */
require get_template_directory() . '/inc/scaffolding.php';

//theme support woocomerce 
add_theme_support('woocommerce');

// Make Font Awesome available
add_action( 'wp_enqueue_scripts', 'enqueue_font_awesome' );
function enqueue_font_awesome() {
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' );
}

// Add woc single product image gallery slider
add_action( 'after_setup_theme', 'wc_product_image_slider' );
 
function wc_product_image_slider() {
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}

// Remove Lightbox
function remove_wc_gallery_lightbox() {
	remove_theme_support( 'wc-product-gallery-lightbox' );
} add_action( 'after_setup_theme', 'remove_wc_gallery_lightbox', 100 );
	
remove_theme_support( 'wc-product-gallery-zoom' ); // REMOVE GALLERY ZOOM
remove_theme_support( 'wc-product-gallery-lightbox' ); // REMOVE GALLERY LIGHTBOX
remove_theme_support( 'wc-product-gallery-slider' ); // REMOVE GALLERY SLIDER

// add search functionality
add_filter('wp_nav_menu_items', 'add_search_form', 10, 2);
function add_search_form($items, $args) {
	if( $args->theme_location != 'category-menu') {
		return $items;
	} elseif( is_home() || is_search() || is_single() && 'post' == get_post_type() ) {
		$items .= '<li class="search"><a class="search_icon"><i id="search_icon" class="fa fa-search"></i></a><div style="display:none;" class="navsearchform"><form method="get" class="search-form" action="/"><fieldset><input type=text name="s" ><i class="fa fa-search  search_iconx"></i></fieldset></form></div></li>';
		return $items;
	} else {
		return $items;
	}
}	

/**
 * Place a cart icon with number of items in the menu bar
 *
 * Source: http://wordpress.org/plugins/woocommerce-menu-bar-cart/
 */
add_filter('wp_nav_menu_items','sk_wcmenucart', 10, 2);
function sk_wcmenucart($menu, $args) {
	// Check if WooCommerce is active and add a new item to a menu assigned to Primary Navigation Menu location
	if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) || 'category-menu' !== $args->theme_location )

	return $menu;
	ob_start();
	global $woocommerce;

	$viewing_cart = __('View your shopping cart', 'gns');
	$start_shopping = __('Start shopping', 'gns');
	$cart_url = wc_get_cart_url();
	$shop_page_url = get_permalink( wc_get_page_id( 'shop' ) );
	$cart_contents_count = $woocommerce->cart->cart_contents_count;
	$cart_contents = sprintf(_n('%d ', '%d ', $cart_contents_count, 'gns'), $cart_contents_count);

	if ($cart_contents_count == 0) {
		$menu_item = '<li class="right"><a class="wcmenucart-contents" href="'. $cart_url .'" title="'. $start_shopping .'">';
	} else {
		$menu_item = '<li class="right"><a class="wcmenucart-contents" href="'. $cart_url .'" title="'. $viewing_cart .'">';
	}

	$menu_item .= '<?xml version="1.0" encoding="UTF-8"?>
	<svg width="25px" height="22px" viewBox="0 0 25 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<title>icn-ui-cart</title>
		<g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
			<g id="icons/ui/.icon.ui-cart" transform="translate(0.000000, -3.000000)" fill="#C39A6B" fill-rule="nonzero">
				<path d="M10,21 C11.1027778,21 12,21.8972222 12,23 C12,24.1027778 11.1027778,25 10,25 C8.89722222,25 8,24.1027778 8,23 C8,21.8972222 8.89722222,21 10,21 Z M20,21 C21.1027778,21 22,21.8972222 22,23 C22,24.1027778 21.1027778,25 20,25 C18.8972222,25 18,24.1027778 18,23 C18,21.8972222 18.8972222,21 20,21 Z M10,22.3333333 C9.63246529,22.3333333 9.33333333,22.6324653 9.33333333,23 C9.33333333,23.3675347 9.63246529,23.6666667 10,23.6666667 C10.3675347,23.6666667 10.6666667,23.3675347 10.6666667,23 C10.6666667,22.6324653 10.3675347,22.3333333 10,22.3333333 Z M20,22.3333333 C19.6324653,22.3333333 19.3333333,22.6324653 19.3333333,23 C19.3333333,23.3675347 19.6324653,23.6666667 20,23.6666667 C20.3675347,23.6666667 20.6666667,23.3675347 20.6666667,23 C20.6666667,22.6324653 20.3675347,22.3333333 20,22.3333333 Z M5.12696172,3 C5.47028504,3 5.76745048,3.2441406 5.8420279,3.5873047 L5.8420279,3.5873047 L6.36559602,6 L24.2676188,6 C24.4974547,6 24.7139391,6.1105469 24.8524128,6.2984375 C24.9906959,6.4863281 25.0349464,6.7296875 24.9718131,6.9560547 L24.9718131,6.9560547 L22.0421207,17.4560547 C21.9522844,17.7779297 21.6648465,18 21.3379264,18 L21.3379264,18 L8.0547468,18.0001953 C7.65172332,18.0011719 7.32423102,18.3371094 7.32423102,18.75 C7.32423102,19.1634765 7.65286775,19.5 8.05665412,19.5 L8.05665412,19.5 L21.3379264,19.5 C21.7424757,19.5 22.0703495,19.8357422 22.0703495,20.25 C22.0703495,20.6642578 21.7424757,21 21.3379264,21 L21.3379264,21 L8.05665412,21 C6.84510424,21 5.85938482,19.990625 5.85938482,18.75 C5.85938482,17.8267578 6.40545966,17.0320312 7.18384993,16.6855469 C7.04499471,16.0458984 4.63219468,4.92753905 4.53949735,4.5 L4.53949735,4.5 L0.732423102,4.5 C0.327873792,4.5 0,4.1642578 0,3.75 C0,3.3357422 0.327873792,3 0.732423102,3 L0.732423102,3 Z M23.296586,7.5 L6.69099021,7.5 L8.64411849,16.5 L20.7853666,16.5 L23.296586,7.5 Z" id="icn-ui-cart"></path>
			</g>
		</g>
	</svg> ';

	$menu_item .= '<span>'.$cart_contents.'</span>';
	$menu_item .= '</a></li>';
	
	echo $menu_item;
	$social = ob_get_clean();
	return $menu . $social;
}

function bbloomer_cart_refresh_update_qty() {
    if (is_cart()) {
        ?>
     <script type="text/javascript"> 
        jQuery('div.woocommerce').on('change', 'input.qty', function(){
            setTimeout(function() {
                jQuery('[name="update_cart"]').trigger('click');
            }, 100 );
        });
		</script> 
        <?php
    }
} add_action( 'wp_footer', 'bbloomer_cart_refresh_update_qty', 20 );

add_action( 'wp_footer', 'bbloomer_add_cart_quantity_plus_minus' );
  
function bbloomer_add_cart_quantity_plus_minus() {
 
   if ( ! is_product() && ! is_cart() ) return;
    
   wc_enqueue_js( "   
           
      $(document).on( 'click', 'button.qty-plus, button.qty-minus', function(e) {
  
         var body = $( 'body' );
         var miniCartCount = $( '.wcmenucart-contents' ).find( 'span' );
         var miniCartCountVal = $( '.wcmenucart-contents' ).find( 'span' ).html();
         var qty = $( this ).parent( '.quantity' ).find( '.qty' );
         var val = parseFloat(qty.val());
         var max = parseFloat(qty.attr( 'max' ));
         var min = parseFloat(qty.attr( 'min' ));
         var step = parseFloat(qty.attr( 'step' ));
 
         if ( $( this ).is( '.qty-plus' ) ) {
            if ( max && ( max <= val ) ) {
               qty.val( max ).change();
            } else {
               qty.val( val + step ).change();
               if(body.hasClass('woocommerce-cart')) {
	               miniCartCount.html( parseInt(miniCartCountVal) + parseInt(step) );
	           }
            }
         } else {
            if ( min && ( min >= val ) ) {
               qty.val( min ).change();
            } else if ( val > 1 ) {
               qty.val( val - step ).change();
               if(body.hasClass('woocommerce-cart')) {
	               miniCartCount.html( parseInt(miniCartCountVal) - parseInt(step) );
	           }
            }
         }

         e.preventDefault();
 
      });

      $(document).on( 'click', '.remove', function() {
      	var miniCartCount = $( '.wcmenucart-contents' ).find( 'span' );
		var miniCartCountVal = $( '.wcmenucart-contents' ).find( 'span' ).html();
		var qty = parseFloat($( this ).parent( '.product-remove' ).siblings('.product-wrapper').find( '.qty' ).val());

		miniCartCount.html( parseInt(miniCartCountVal) - parseInt(qty) );
      });
        
   " );
}

//Hide shipping calculator fields on cart
// Disable City
add_filter( 'woocommerce_shipping_calculator_enable_city', '__return_false' );
// Disable Postcode
add_filter( 'woocommerce_shipping_calculator_enable_postcode', '__return_false' );


// Product variation support
add_action('woocommerce_after_shop_loop_item_title', 'get_product_variations', 5);
function get_product_variations() {
	global $product;
    if( $product->is_type( 'variable' ) ){
		$variations = $product->get_available_variations();
		// die(var_dump($variations));
		foreach (array_slice($variations,0,3) as $variant) {
			$price = $variant['display_price'];
			$quantity = intval($variant['attributes']['attribute_pa_weight']) . ' Pounds';
			// $quantity = $variant['weight'];
			echo '<p><span class="quantity">'.$quantity . '</span> - <span class="q-price">$' . $price  . '</span></p>';
		}
	}
	else {
		echo "not variable";
	}
	return $product;
}

// WooCommerce Widget
add_action('woocommerce_widget', 'get_sidebar_variation');
function get_sidebar_variation() {
	global $product;
    if( $product->is_type( 'variable' ) ){
		$variations = $product->get_available_variations();
		foreach (array_slice($variations,0,3) as $variant) {
			$price = $variant['display_price'];
			$quantity = $variant['attributes']['attribute_pa_weight'];
			echo '<span>'.$quantity . ' : <strong>$' . $price  . '</strong></span>';
		}
	}
	else {
		echo "not variable";
	}
}

// Hide coupon field on the cart page
function disable_coupon_field_on_cart( $enabled ) {
	if ( is_cart() ) {
		$enabled = false;
	}
	return $enabled;
}

add_filter( 'woocommerce_coupons_enabled', 'disable_coupon_field_on_cart', 20 ); // DISABLE COUPONS IN CART
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 ); // REMOVE SORT BY DROPDOWN IN SHOP
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 ); // REMOVE PRODUCT COUNT IN SHOP
 
// Theme Options
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'promotion banner Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));	
}

// Remove Product Image Zoom
function remove_product_zoom_support() {
    remove_theme_support( 'wc-product-gallery-zoom' );
} add_action( 'wp', 'remove_product_zoom_support', 100 );

// Removes SKU From Single Product Page
function sv_remove_product_page_skus( $enabled ) {
    if ( ! is_admin() && is_product() ) {
        return false;
    }

    return $enabled;
} add_filter( 'wc_product_sku_enabled', 'sv_remove_product_page_skus' );

// Thumbnail Support
add_theme_support( 'post-thumbnails' );

// Register template redirect action callback 
add_action('template_redirect', 'meks_remove_wp_archives');
 
// Remove archives
function meks_remove_wp_archives(){
  //If we are on category or tag or date or author archive
  if( is_category() || is_tag() || is_date() || is_author() ) {
    global $wp_query;
    $wp_query->set_404(); //set to 404 not found page
  }
}

// Add title attributes to content images
function inserted_image_titles( $html, $id ) {
	$attachment = get_post($id);
	$thetitle = $attachment->post_title;
	$thedescription = $attachment->post_description;
	$image_alt = get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true);
	return str_replace('<img', '<img alt="' . $image_alt . '" title="' . $thetitle . '" ' .'description="' . $thedescription . '" '  , $html);
} add_filter( 'media_send_to_editor', 'inserted_image_titles', 15, 2 );

// Add title attributes to featured images
function featured_image_titles($attr, $attachment = null){
	$attr['title'] = get_post($attachment->ID)->post_title;
	return $attr;
} add_filter('wp_get_attachment_image_attributes', 'featured_image_titles', 10, 2);


// Hope this works
// if ( ! function_exists( 'gn_dropdown_variation_attribute_options' ) ) {

// 	/**
// 	 * Output a list of variation attributes for use in the cart forms.
// 	 *
// 	 * @param array $args Arguments.
// 	 * @since 2.4.0
// 	 */

// 	function your_woovr_variation_radio_selector( $selector, $product_id, $checked ) {
// 	   return '<div class="woovr-variation-selector"><label><input class="option-input radio" type="radio" name="woovr_variation_' . $product_id . '" ' . $checked . '/></label></div>';
// 	}

// 	function gn_dropdown_variation_attribute_options( $args = array() ) {
// 		$args = wp_parse_args(
// 			apply_filters( 'woocommerce_dropdown_variation_attribute_options_args', $args ),
// 			array(
// 				'options'          => false,
// 				'attribute'        => false,
// 				'product'          => false,
// 				'selected'         => false,
// 				'name'             => '',
// 				'id'               => '',
// 				'class'            => '',
// 				'show_option_none' => __( 'Choose an TEST', 'woocommerce' ),
// 			)
// 		);

// 		// Get selected value.
// 		if ( false === $args['selected'] && $args['attribute'] && $args['product'] instanceof WC_Product ) {
// 			$selected_key = 'attribute_' . sanitize_title( $args['attribute'] );
// 			// phpcs:disable WordPress.Security.NonceVerification.Recommended
// 			$args['selected'] = isset( $_REQUEST[ $selected_key ] ) ? wc_clean( wp_unslash( $_REQUEST[ $selected_key ] ) ) : $args['product']->get_variation_default_attribute( $args['attribute'] );
// 			// phpcs:enable WordPress.Security.NonceVerification.Recommended
// 		}

// 		$options               = $args['options'];
// 		$checked               = $args['checked'];
// 		$product               = $args['product'];
// 		$attribute             = $args['attribute'];
// 		$name                  = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute );
// 		$id                    = $args['id'] ? $args['id'] : sanitize_title( $attribute );
// 		$class                 = $args['class'];
// 		$show_option_none      = (bool) $args['show_option_none'];
// 		$show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Choose an option', 'woocommerce' ); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options.

// 		if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) {
// 			$attributes = $product->get_variation_attributes();
// 			$options    = $attributes[ $attribute ];
// 		}

// 		$html  = '<div id="' . esc_attr( $id ) . '" class="' . esc_attr( $class ) . '" name="' . esc_attr( $name ) . '" data-attribute_name="attribute_' . esc_attr( sanitize_title( $attribute ) ) . '" data-show_option_none="' . ( $show_option_none ? 'yes' : 'no' ) . '">';
// 		$html .= '<option value="">' . esc_html( $show_option_none_text ) . '</option>';

// 		if ( ! empty( $options ) ) {
// 			if ( $product && taxonomy_exists( $attribute ) ) {
// 				// Get terms if this is a taxonomy - ordered. We need the names too.
// 				$terms = wc_get_product_terms(
// 					$product->get_id(),
// 					$attribute,
// 					array(
// 						'fields' => 'all',
// 					)
// 				);

// 				foreach ( $terms as $term ) {
// 					if ( in_array( $term->slug, $options, true ) ) {
// 						$html .= '<option value="' . esc_attr( $term->slug ) . '" ' . selected( sanitize_title( $args['selected'] ), $term->slug, false ) . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name, $term, $attribute, $product ) ) . '</option>';
// 					}
// 				}
// 			} else {
// 				foreach ( $options as $option ) {
// 					// This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
// 					$selected = sanitize_title( $args['selected'] ) === $args['selected'] ? selected( $args['selected'], sanitize_title( $option ), false ) : selected( $args['selected'], $option, false );
// 					$html    .= '<label for="' . esc_attr( $option ) . '"></label><input value="' . esc_attr( $option ) . '" ' . $selected . ' class="option-input radio" type="radio" name="woovr_variation_' . esc_html( $option ) . '" ' . $checked . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option, null, $attribute, $product ) ) . '</input>';
// 				}
// 			}
// 		}

// 		$html .= '</div>';

// 		// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
// 		echo apply_filters( 'woocommerce_dropdown_variation_attribute_options_html', $html, $args );

// 		add_filter( 'woovr_variation_radio_selector', 'your_woovr_variation_radio_selector', 99, 3 );

// 		echo apply_filters( 'woovr_variation_radio_selector', 'your_woovr_variation_radio_selector', 99, 3 );
// 	}
// }

// defined( 'ABSPATH' ) || exit;

// ! defined( 'WOOVR_PATH' ) && define( 'WOOVR_PATH', plugin_dir_path( __FILE__ ) );
// ! defined( 'WOOVR_URI' ) && define( 'WOOVR_URI', plugin_dir_url( __FILE__ ) );
// ! defined( 'WPC_URI' ) && define( 'WPC_URI', WOOVR_URI );


// if ( ! function_exists( 'woovr_init' ) ) {
// 	add_action( 'plugins_loaded', 'woovr_init', 11 );

// 	function woovr_init() {
// 		load_plugin_textdomain( 'wpc-variations-radio-buttons', false, basename( __DIR__ ) . '/languages/' );

// 		if ( ! class_exists( 'WPClever_Woovr' ) && class_exists( 'WC_Product' ) ) {
			
// 			class WPClever_Woovr {
// 				function __construct() {
// 					// settings page
// 					add_action( 'admin_menu', array( $this, 'woovr_admin_menu' ) );

// 					// settings link
// 					add_filter( 'plugin_action_links', array( $this, 'woovr_action_links' ), 10, 2 );
// 					add_filter( 'plugin_row_meta', array( $this, 'woovr_row_meta' ), 10, 2 );

// 					// enqueue backend scripts
// 					add_action( 'admin_enqueue_scripts', array( $this, 'woovr_admin_enqueue_scripts' ), 99 );

// 					// enqueue frontend scripts
// 					add_action( 'wp_enqueue_scripts', array( $this, 'woovr_enqueue_scripts' ), 99 );

// 					// product data tabs
// 					add_filter( 'woocommerce_product_data_tabs', array( $this, 'woovr_product_data_tabs' ), 10, 1 );
// 					add_action( 'woocommerce_product_data_panels', array( $this, 'woovr_product_data_panels' ) );

// 					// functions
// 					if ( get_option( '_woovr_active', 'yes' ) !== 'yes_wpc' ) {
// 						add_filter( 'woocommerce_post_class', array( $this, 'woovr_post_class' ), 99, 2 );
// 						add_action( 'woocommerce_before_variations_form', array(
// 							$this,
// 							'woovr_before_variations_form'
// 						) );
// 					}

// 					// custom variation name & image
// 					add_action( 'woocommerce_product_after_variable_attributes', array(
// 						$this,
// 						'woovr_variation_settings_fields'
// 					), 10, 3 );
// 					add_action( 'woocommerce_save_product_variation', array(
// 						$this,
// 						'woovr_save_variation_settings'
// 					), 10, 2 );
// 					add_filter( 'woocommerce_product_variation_get_name', array(
// 						$this,
// 						'woovr_product_variation_get_name'
// 					), 99, 2 );
// 				}

// 				function woovr_admin_enqueue_scripts() {
// 					// wp_enqueue_style( 'woovr-backend', WOOVR_URI . 'assets/css/backend.css' );
// 					wp_enqueue_script( 'woovr-backend', WOOVR_URI . 'assets/scripts/backend.js', array( 'jquery' ), WOOVR_VERSION, true );
// 				}

// 				function woovr_enqueue_scripts() {
// 					// woovr
// 					// wp_enqueue_style( 'woovr-frontend', WOOVR_URI . 'assets/css/frontend.css' );
// 					wp_enqueue_script( 'woovr-frontend', WOOVR_URI . 'assets/scripts/frontend.js', array( 'jquery' ), WOOVR_VERSION, true );
// 				}

// 				function woovr_product_variation_get_name( $name, $product ) {
// 					if ( $custom_name = get_post_meta( $product->get_id(), 'woovr_name', true ) ) {
// 						if ( ! empty( $custom_name ) ) {
// 							return $custom_name;
// 						}
// 					}
				
// 					return $name;
// 				}
				
// 				function woovr_post_class( $classes, $product ) {
// 					$product_id        = $product->get_id();
// 					$active            = get_option( '_woovr_active', 'yes' );
// 					$show_price        = get_option( '_woovr_show_price', 'yes' );
// 					$show_availability = get_option( '_woovr_show_availability', 'yes' );
// 					$show_description  = get_option( '_woovr_show_description', 'yes' );
// 					$_active           = get_post_meta( $product_id, '_woovr_active', true ) ?: 'default';
				
// 					if ( $_active === 'yes' ) {
// 						// overwrite settings
// 						$show_price        = get_post_meta( $product_id, '_woovr_show_price', true ) ?: $show_price;
// 						$show_availability = get_post_meta( $product_id, '_woovr_show_availability', true ) ?: $show_availability;
// 						$show_description  = get_post_meta( $product_id, '_woovr_show_description', true ) ?: $show_description;
// 					}
				
// 					if ( ( $_active === 'yes' ) || ( ( $_active === 'default' ) && ( $active === 'yes' ) ) ) {
// 						$classes[] = 'woovr-active';
				
// 						if ( $show_price === 'yes' ) {
// 							$classes[] = 'woovr-show-price';
// 						}
				
// 						if ( $show_availability === 'yes' ) {
// 							$classes[] = 'woovr-show-availability';
// 						}
				
// 						if ( $show_description === 'yes' ) {
// 							$classes[] = 'woovr-show-description';
// 						}
// 					}
				
// 					return $classes;
// 				}
				
// 				function woovr_data_attributes( $attrs ) {
// 					$attrs_arr = array();
				
// 					foreach ( $attrs as $key => $attr ) {
// 						$attrs_arr[] = 'data-' . sanitize_title( $key ) . '="' . esc_attr( $attr ) . '"';
// 					}
				
// 					return implode( ' ', $attrs_arr );
// 				}
				
// 				static function woovr_is_purchasable( $product ) {
// 					return $product->is_purchasable() && $product->is_in_stock() && $product->has_enough_stock( 1 );
// 				}
				
// 				public static function woovr_variations_form( $product ) {
// 					$product_id = $product->get_id();
// 					$_active    = get_post_meta( $product_id, '_woovr_active', true ) ?: 'default';
				
// 					$selector           = get_option( '_woovr_selector', 'default' );
// 					$variation_name     = get_option( '_woovr_variation_name', 'default' );
// 					$show_clear         = get_option( '_woovr_show_clear', 'yes' );
// 					$show_image         = get_option( '_woovr_show_image', 'yes' );
// 					$show_price         = get_option( '_woovr_show_price', 'yes' );
// 					$show_availability  = get_option( '_woovr_show_availability', 'yes' );
// 					$show_description   = get_option( '_woovr_show_description', 'yes' );
// 					$hide_unpurchasable = get_option( '_woovr_hide_unpurchasable', 'no' );
				
// 					if ( $_active === 'yes' ) {
// 						// overwrite settings
// 						$selector          = get_post_meta( $product_id, '_woovr_selector', true ) ?: $selector;
// 						$variation_name    = get_post_meta( $product_id, '_woovr_variation_name', true ) ?: $variation_name;
// 						$show_image        = get_post_meta( $product_id, '_woovr_show_image', true ) ?: $show_image;
// 						$show_price        = get_post_meta( $product_id, '_woovr_show_price', true ) ?: $show_price;
// 						$show_availability = get_post_meta( $product_id, '_woovr_show_availability', true ) ?: $show_availability;
// 						$show_description  = get_post_meta( $product_id, '_woovr_show_description', true ) ?: $show_description;
// 					}
				
// 					$df_attrs_arr = array();
// 					$df_attrs     = $product->get_default_attributes();
				
// 					if ( ! empty( $df_attrs ) ) {
// 						foreach ( $df_attrs as $key => $val ) {
// 							$df_attrs_arr[ 'attribute_' . $key ] = $val;
// 						}
// 					}
				
// 					$children = $product->get_children();
				
// 					if ( is_array( $children ) && count( $children ) > 0 ) {
// 						echo '<div class="woovr-variations ' . esc_attr( 'woovr-variations-' . $selector ) . '" data-click="0" data-description="' . esc_attr( $show_description ) . '">';
				
// 						if ( $selector === 'default' ) {
// 							// show choose an option
// 							if ( $show_clear === 'yes' ) {
// 								$data_attrs = apply_filters( 'woovr_data_attributes_option_none', array(
// 									'id'            => 0,
// 									'sku'           => '',
// 									'purchasable'   => 'no',
// 									'attrs'         => '',
// 									'price'         => 0,
// 									'regular-price' => 0,
// 									'pricehtml'     => '',
// 									'availability'  => ''
// 								) );
				
// 								$df_checked = empty( $df_attrs ) ? 'checked' : '';
				
// 								echo '<div class="woovr-variation woovr-variation-radio ' . ( empty( $df_attrs ) ? 'woovr-variation-active' : '' ) . '" ' . self::woovr_data_attributes( $data_attrs ) . '>';
// 								echo apply_filters( 'woovr_variation_radio_selector', '<div class="woovr-variation-selector"><input type="radio" name="woovr_variation_' . $product_id . '" ' . $df_checked . '/></div>', $product_id, $df_checked );
				
// 								if ( $show_image === 'yes' ) {
// 									echo '<div class="woovr-variation-image">' . apply_filters( 'woovr_clear_image', wc_placeholder_img(), $product ) . '</div>';
// 								}
				
// 								echo '<div class="woovr-variation-name">' . esc_html__( 'Choose an option', 'wpc-variations-radio-buttons' ) . '</div>';
				
// 								if ( $show_price === 'yes' ) {
// 									echo '<div class="woovr-variation-price">' . apply_filters( 'woovr_clear_description', '', $product ) . '</div>';
// 								}
				
// 								echo '</div><!-- /woovr-variation -->';
// 							}
				
// 							// radio buttons
// 							foreach ( $children as $child ) {
// 								$child_product = wc_get_product( $child );
				
// 								if ( ! $child_product || ! $child_product->variation_is_visible() ) {
// 									continue;
// 								}
				
// 								if ( ( $hide_unpurchasable === 'yes' ) && ! self::woovr_is_purchasable( $child_product ) ) {
// 									continue;
// 								}
				
// 								$child_attrs   = htmlspecialchars( json_encode( $child_product->get_variation_attributes() ), ENT_QUOTES, 'UTF-8' );
// 								$child_checked = $child_product->get_variation_attributes() == $df_attrs_arr ? 'checked' : '';
// 								$child_class   = 'woovr-variation woovr-variation-radio';
				
// 								if ( $child_checked === 'checked' ) {
// 									$child_class .= ' woovr-variation-active';
// 								}
				
// 								if ( $variation_name === 'formatted_label' ) {
// 									$child_name = wc_get_formatted_variation( $child_product, true, true, false );
// 								} elseif ( $variation_name === 'formatted' ) {
// 									$child_name = wc_get_formatted_variation( $child_product, true, false, false );
// 								} else {
// 									// default
// 									$child_name = $child_product->get_name();
// 								}
				
// 								if ( $variation_name !== 'default' && $custom_name = get_post_meta( $child, 'woovr_name', true ) ) {
// 									if ( ! empty( $custom_name ) ) {
// 										$child_name = $custom_name;
// 									}
// 								}
				
// 								if ( $child_product->get_image_id() ) {
// 									$child_image     = wp_get_attachment_image_src( $child_product->get_image_id(), 'thumbnail' );
// 									$child_image_src = get_post_meta( $child, 'woovr_image', true ) ?: $child_image[0];
// 									$child_image_src = esc_url( apply_filters( 'woovr_variation_image_src', $child_image_src, $child_product ) );
// 								} else {
// 									$child_image_src = esc_url( apply_filters( 'woovr_variation_image_src', wc_placeholder_img_src(), $child_product ) );
// 								}
				
// 								$data_attrs = apply_filters( 'woovr_data_attributes', array(
// 									'id'            => $child,
// 									'sku'           => $child_product->get_sku(),
// 									'purchasable'   => self::woovr_is_purchasable( $child_product ) ? 'yes' : 'no',
// 									'attrs'         => $child_attrs,
// 									'price'         => wc_get_price_to_display( $child_product ),
// 									'regular-price' => wc_get_price_to_display( $child_product, array( 'price' => $child_product->get_regular_price() ) ),
// 									'pricehtml'     => htmlentities( $child_product->get_price_html() ),
// 									'imagesrc'      => $child_image_src,
// 									'availability'  => htmlentities( wc_get_stock_html( $child_product ) )
// 								), $child_product );
				
// 								echo '<div class="' . esc_attr( $child_class ) . '" ' . self::woovr_data_attributes( $data_attrs ) . '>';
// 								echo apply_filters( 'woovr_variation_radio_selector', '<div class="woovr-variation-selector"><input type="radio" name="woovr_variation_' . $product_id . '" ' . $child_checked . '/></div>', $product_id, $child_checked );
				
// 								if ( $show_image === 'yes' ) {
// 									echo '<div class="woovr-variation-image"><img src="' . $child_image_src . '"/></div>';
// 								}
				
// 								echo '<div class="woovr-variation-info">';
// 								echo '<div class="woovr-variation-name">' . apply_filters( 'woovr_variation_name', $child_name, $child_product ) . '</div>';
				
// 								if ( $show_price === 'yes' ) {
// 									echo '<div class="woovr-variation-price">' . apply_filters( 'woovr_variation_price', $child_product->get_price_html(), $child_product ) . '</div>';
// 								}
				
// 								if ( $show_availability === 'yes' ) {
// 									echo '<div class="woovr-variation-availability">' . apply_filters( 'woovr_variation_availability', wc_get_stock_html( $child_product ), $child_product ) . '</div>';
// 								}
				
// 								if ( $show_description === 'yes' ) {
// 									echo '<div class="woovr-variation-description">' . apply_filters( 'woovr_variation_description', $child_product->get_description(), $child_product ) . '</div>';
// 								}
				
// 								echo '</div><!-- /woovr-variation-name -->';
// 								echo '</div><!-- /woovr-variation-info -->';
// 							}
// 						} else {
// 							// dropdown
// 							echo '<div class="woovr-variation woovr-variation-dropdown">';
				
// 							if ( ( $selector === 'select' ) && ( $show_image === 'yes' ) ) {
// 								echo '<div class="woovr-variation-image">' . apply_filters( 'woovr_clear_image', wc_placeholder_img(), $product ) . '</div>';
// 							}
				
// 							echo '<div class="woovr-variation-selector"><select class="woovr-variation-select" id="woovr-variation-select-' . esc_attr( $product_id ) . '">';
				
// 							// show choose an option
// 							if ( $show_clear === 'yes' ) {
// 								if ( ( $selector === 'select' ) ) {
// 									$imagesrc = esc_url( apply_filters( 'woovr_clear_image_src', wc_placeholder_img_src(), $product ) );
// 								} else {
// 									$imagesrc = esc_url( apply_filters( 'woovr_clear_image_src', '', $product ) );
// 								}
				
// 								$data_attrs = apply_filters( 'woovr_data_attributes_option_none', array(
// 									'id'            => 0,
// 									'sku'           => '',
// 									'purchasable'   => 'no',
// 									'attrs'         => '',
// 									'price'         => 0,
// 									'regular-price' => 0,
// 									'pricehtml'     => '',
// 									'imagesrc'      => $imagesrc,
// 									'description'   => htmlentities( apply_filters( 'woovr_clear_description', '', $product ) ),
// 									'availability'  => ''
// 								) );
// 								echo '<option value="0" ' . self::woovr_data_attributes( $data_attrs ) . '>' . esc_html__( 'Choose an option', 'wpc-variations-radio-buttons' ) . '</option>';
// 							}
				
// 							foreach ( $children as $child ) {
// 								$child_product = wc_get_product( $child );
				
// 								if ( ! $child_product || ! $child_product->variation_is_visible() ) {
// 									continue;
// 								}
				
// 								if ( ( $hide_unpurchasable === 'yes' ) && ! self::woovr_is_purchasable( $child_product ) ) {
// 									continue;
// 								}
				
// 								if ( $variation_name === 'formatted_label' ) {
// 									$child_name = wc_get_formatted_variation( $child_product, true, true, false );
// 								} elseif ( $variation_name === 'formatted' ) {
// 									$child_name = wc_get_formatted_variation( $child_product, true, false, false );
// 								} else {
// 									// default
// 									$child_name = $child_product->get_name();
// 								}
				
// 								if ( $variation_name !== 'default' && $custom_name = get_post_meta( $child, 'woovr_name', true ) ) {
// 									if ( ! empty( $custom_name ) ) {
// 										$child_name = $custom_name;
// 									}
// 								}
				
// 								if ( $child_product->get_image_id() ) {
// 									$child_image     = wp_get_attachment_image_src( $child_product->get_image_id(), 'thumbnail' );
// 									$child_image_src = get_post_meta( $child, 'woovr_image', true ) ?: $child_image[0];
// 									$child_image_src = esc_url( apply_filters( 'woovr_variation_image_src', $child_image_src, $child_product ) );
// 								} else {
// 									$child_image_src = esc_url( apply_filters( 'woovr_variation_image_src', wc_placeholder_img_src(), $child_product ) );
// 								}
				
// 								$child_attrs    = htmlspecialchars( json_encode( $child_product->get_variation_attributes() ), ENT_QUOTES, 'UTF-8' );
// 								$child_selected = $child_product->get_variation_attributes() == $df_attrs_arr ? 'selected' : '';
// 								$child_info     = '';
				
// 								if ( $show_price === 'yes' ) {
// 									$child_info .= '<span class="woovr-variation-price">' . apply_filters( 'woovr_variation_price', $child_product->get_price_html(), $child_product ) . '</span>';
// 								}
				
// 								if ( $show_availability === 'yes' ) {
// 									$child_info .= '<span class="woovr-variation-availability">' . apply_filters( 'woovr_variation_availability', wc_get_stock_html( $child_product ), $child_product ) . '</span>';
// 								}
				
// 								if ( $show_description === 'yes' ) {
// 									$child_info .= '<span class="woovr-variation-description">' . apply_filters( 'woovr_variation_description', $child_product->get_description(), $child_product ) . '</span>';
// 								}
				
// 								$data_attrs = apply_filters( 'woovr_data_attributes', array(
// 									'id'            => $child,
// 									'sku'           => $child_product->get_sku(),
// 									'purchasable'   => self::woovr_is_purchasable( $child_product ) ? 'yes' : 'no',
// 									'attrs'         => $child_attrs,
// 									'price'         => wc_get_price_to_display( $child_product ),
// 									'regular-price' => wc_get_price_to_display( $child_product, array( 'price' => $child_product->get_regular_price() ) ),
// 									'pricehtml'     => htmlentities( $child_product->get_price_html() ),
// 									'imagesrc'      => esc_url( apply_filters( 'woovr_variation_image_src', $child_image_src, $child_product ) ),
// 									'description'   => htmlentities( apply_filters( 'woovr_variation_info', $child_info, $child_product ) ),
// 									'availability'  => htmlentities( wc_get_stock_html( $child_product ) )
// 								), $child_product );
				
// 								echo '<option value="' . $child . '" ' . self::woovr_data_attributes( $data_attrs ) . ' ' . $child_selected . '>' . apply_filters( 'woovr_variation_name', $child_name, $child_product ) . '</option>';
// 							}
				
// 							echo '</select></div><!-- /woovr-variation-selector -->';
				
// 							if ( ( $selector === 'select' ) && ( $show_price === 'yes' ) ) {
// 								echo '<div class="woovr-variation-price"></div>';
// 							}
				
// 							echo '</div><!-- /woovr-variation -->';
// 						}
				
// 						echo '</div><!-- /woovr-variations -->';
// 					}
// 				}
// 			}
// 			new WPClever_Woovr();
// 		}
// 	}
// }
