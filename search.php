<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Gourmet Nuts & Dried Fruits
 */

get_header(); ?>

<main id="main" class="site-main">
<div class="full-width blog-archive">
		<div class="container">
			<div class="display-flex grid-wrapper">
				<div class="left-two-thirds">
					<?php
					if ( have_posts() ) :

						if ( is_home() && ! is_front_page() ) :
					?>

					<header>
						<h1 class="page-title screen-reader-text"><?php /* translators: the term(s) searched */ printf( esc_html__( 'Search Results for: %s', 'gns' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
					</header>

						<?php
						endif;

						/* Start the Loop */
						while ( have_posts() ) :
							the_post();

							/*
								* Include the Post-Format-specific template for the content.
								* If you want to override this in a child theme, then include a file
								* called content-___.php (where ___ is the Post Format name) and that will be used instead.
								*/
							//get_template_part( 'template-parts/content', get_post_format() );
							get_template_part( 'template-parts/content', 'blog-feed' );

						endwhile;

						hyd__display_numeric_pagination();

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>
				</div>

				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>
	</main><!-- #main -->

<?php get_footer(); ?>
