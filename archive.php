<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gourmet Nuts & Dried Fruits
 */

get_header(); ?>

<main id="main" class="site-main">
	<div class="full-width blog-archive">
			<div class="container">
				<div class="display-flex grid-wrapper wrap">
					<div class="left-two-thirds">
						<?php if ( have_posts() ) : ?>

							<header class="page-header">
								<?php
									the_archive_title( '<h1 class="page-title">', '</h1>' );
									the_archive_description( '<div class="archive-description">', '</div>' );
								?>
							</header><!-- .page-header -->

							<?php
							/* Start the Loop */
							while ( have_posts() ) :
								the_post();

								/*
									* Include the Post-Format-specific template for the content.
									* If you want to override this in a child theme, then include a file
									* called content-___.php (where ___ is the Post Format name) and that will be used instead.
									*/
								// get_template_part( 'template-parts/content', get_post_format() );
								get_template_part( 'template-parts/content', 'blog-feed' );

							endwhile;

							hyd__display_numeric_pagination();

						else :

							get_template_part( 'template-parts/content', 'none' );

						endif;
						?>
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
	</div>
</div>
	</main><!-- #main -->
<?php get_footer(); ?>
