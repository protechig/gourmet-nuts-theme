<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, hyd__scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gourmet Nuts & Dried Fruits
 */

get_header(); ?>

	<main id="main" class="site-main container">

		<?php do_action( 'hyd__scaffolding_content' ); ?>

	</main><!-- #main -->

<?php get_footer(); ?>
