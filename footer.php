<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gourmet Nuts & Dried Fruits
 */

?>

	<!-- STICKY ON SCROLL START -->
	<script type="text/javascript">
			window.onscroll = function() {myFunction()};

			var header = document.getElementById("header");
			var sticky = header.offsetTop;

			function myFunction() {
			if (window.pageYOffset > sticky) {
				header.classList.add("sticky");
			} else {
				header.classList.remove("sticky");
				}
			}
	</script>
	<!-- STICKY ON SCROLL END -->
	
	<footer class="site-footer background-gallery">
		<nav class="footer-navigation " aria-label="<?php esc_attr_e( 'footer Navigation', 'gns' ); ?>">	
				<?php
				wp_nav_menu(
					array(
						'fallback_cb'    => false,
						'theme_location' => 'footer',
						'menu_id'        => 'footer-menu',
						'menu_class'     => 'footer-nav wrap',
						'container'      => false,
					)
				);
				?>	
		</nav>
		<!-- #footer-navigation -->

		<div class="footer-button">
			<div class="wrap">
				<?php
					$button_icon = get_field('button_icon', 'option');
					$button_icon_hover = get_field('button_icon_hover', 'option');
					$button_url = get_field('button_url', 'option');
					$text = get_field('text', 'option');

					if( $button_icon and $button_icon_hover and $button_url ):

						// Image variables.
						$url = $button_url;
						
						$title = $button_icon['title'];
						$alt = $button_icon['alt'];
						$caption = $button_icon['caption'];

						$title_2 = $button_icon_hover['title_2'];
						$alt_2 = $button_icon_hover['alt_2'];
						$caption_2 = $button_icon_hover['caption_2'];

						// Thumbnail size attributes.
						$size = 'thumbnail';
						$thumb = $button_icon['sizes'][ $size ];
						$width = $button_icon['sizes'][ $size . '-width' ];
						$height = $button_icon['sizes'][ $size . '-height' ];

						$thumb_2 = $button_icon_hover['sizes'][ $size ];
						$width_2 = $button_icon_hover['sizes'][ $size . '-width' ];
						$height_2 = $button_icon_hover['sizes'][ $size . '-height' ];
					?>

					<a href="tel: <?php echo esc_html($url); ?>" title="<?php echo esc_attr($title); ?>" class="button button-icon">


						<span class="footer-button-text"><?php echo esc_html($caption); ?></span>
					</a>
				<?php endif; ?>
			</div>
		</div>

		<div class="site-info">
			<?php hyd__display_copyright_text(); ?>
			<?php hyd__display_social_network_links(); ?>
		</div><!-- .site-info -->
	</footer><!-- .site-footer container -->

	<?php wp_footer(); ?>

	<?php hyd__display_mobile_menu(); ?>

</body>
</html>
