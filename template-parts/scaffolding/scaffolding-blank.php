<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package Gourmet Nuts & Dried Fruits
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'gns' ); ?></h2>
</section>
