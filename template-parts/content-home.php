<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gourmet Nuts & Dried Fruits
 */

?>

	<div class="acf-blocks full-width">
		<?php
			the_content();

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'gns' ),
					'after'  => '</div>',
				)
			);
		?>
	</div><!-- .entry-content -->
