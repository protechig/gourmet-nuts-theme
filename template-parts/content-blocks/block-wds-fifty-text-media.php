<?php
/**
 *  The template used for displaying fifty/fifty text/media.
 *
 * @package Gourmet Nuts & Dried Fruits
 */

// Set up fields.
global $fifty_block, $fifty_alignment, $fifty_classes;
$image_data = get_field( 'media_right' );
$text       = get_field( 'text_primary' );
$bottom_text = get_field( 'full_text_paragraph' );

// Start a <container> with a possible media background.
hyd__display_block_options(
	array(
		'block'     => $fifty_block,
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block grid-container fifty-fifty-block gn-half fifty-text-media' . esc_attr( $fifty_alignment . $fifty_classes ), // Container class.
	)
);
?>
	<div class="wrap ">
	<div class="display-flex  align-top">
		<div class="half">
			<div class="text">
			<?php echo hyd__get_the_content( $text ); // WPCS: XSS OK. ?>
			</div>
		</div>

		<div class="half">
			<a href="<?php the_field('right_image_link'); ?>">
			<?php
			if ( $image_data ) :
				echo wp_get_attachment_image( $image_data['ID'], 'small', true, array( 'class' => 'fifty-image' ) );
				
			endif;
			?>
			</a>
			
		</div>
	</div>
	<?php if ( $bottom_text) : ?>
	<div class="bottom-text">
	<?php
		echo hyd__get_the_content( $bottom_text);
	?>
	</div>
	<?php endif; ?>
	</div>
</section>
