<?php
/**
 *  The template used for displaying quote section.
 *
 * @package Gourmet Nuts & Dried Fruits
 */

// Set up fields.

$description     = get_field( 'description' );
$image_data     = get_field( 'logo' );
$alignment   = hyd__get_block_alignment( $block );
$classes     = hyd__get_block_classes( $block );


	// Start a <container> with possible block options.
	hyd__display_block_options(
		array(
			'block'     => $block,
			'container' => 'section', // Any HTML5 container: section, div, etc...
			'class'     => 'content-block quote-block' . esc_attr( $alignment . $classes ), // Container class.
		)
	);
	?>

        <div class="display-flex wrap dark_bg quote-grid">
            <div class="quote-icon">
                <?php
                if ( $image_data ) :
                    echo wp_get_attachment_image( $image_data['ID'], 'full', true, array( 'class' => 'fifty-image' ) );
                endif;
                ?>
            </div>

            <div class="description">
                <?php echo hyd__get_the_content( $description  ); // WPCS XSS OK. ?>
            </div>

</div>
</section>
