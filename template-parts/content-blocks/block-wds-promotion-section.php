<?php
/**
 * The template used for displaying an accordion block.
 *
 * @package Gourmet Nuts & Dried Fruits
 */

// Set up fields.
$text            = get_field( 'text' );
$alignment       = hyd__get_block_alignment( $block );
$classes         = hyd__get_block_classes( $block );

// Start a <container> with possible block options.
hyd__display_block_options(
	array(
		'block'     => $block,
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block accordion-block' . esc_attr( $alignment . $classes ), // Container class.
	)
);

?>
<?php if ( $text ) : ?>
	<div class="container">
		<h2>
			<?php echo hyd__get_the_content( $text ); // phpcs: xss: ok. ?>
		</h2>
	</div>
<?php endif; ?>
</section>
