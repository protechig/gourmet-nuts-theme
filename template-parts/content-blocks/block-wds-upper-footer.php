<?php
/**
 *  The template used for displaying fifty/fifty text/text.
 *
 * @package Gourmet Nuts & Dried Fruits
 */

// Set up fields.
global $fifty_block, $fifty_alignment, $fifty_classes;
$text_primary   = get_field( 'text_primary' );
$text_secondary = get_field( 'text_secondary' );
$button_text      = get_field( 'phone_button' );
$button_url     = get_field( 'phone_button_url' );
$button_icon       = get_field( 'button_icon' );

// Start a <container> with a possible media background.
hyd__display_block_options(
	array(
		'block'     => $fifty_block,
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'upper-footer-block content-block grid-container fifty-fifty-block fifty-text-only' . esc_attr( $fifty_alignment . $fifty_classes ), // The container class.
	)
);
?>
	<div class="display-flex wrap align-upper">

		<div class=" half contacts">
				<div class="info">
					<?php echo hyd__get_the_content( $text_primary ); // WPCS: XSS OK. ?>
				</div>
				<a href="<?php echo  $button_url; ?>" class=" button btn-orange button-icon">
					<img src="<?php echo $button_icon; ?>">
					<?php echo esc_html( $button_text); ?>
				</a>
		</div>

		<div class=" half locations">
			<div class="details">
				<?php echo hyd__get_the_content( $text_secondary ); // WPCS: XSS OK. ?>
			</div>
		</div>
	</div>
</section>
