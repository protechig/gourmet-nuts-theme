<?php
/**
 *  The template used for displaying fifty/fifty media/text.
 *
 * @package Gourmet Nuts & Dried Fruits
 */

// Set up fields.
global $fifty_block, $fifty_alignment, $fifty_classes;
$image_data = get_field( 'media_left' );
$text       = get_field( 'text_primary' );


// Start a <container> with a possible media background.
hyd__display_block_options(
	array(
		'block'     => $fifty_block,
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block fifty-fifty-block gn-half fifty-media-text' . esc_attr( $fifty_alignment . $fifty_classes ), // The class of the container.
	)
);
?>
	<div class="display-flex wrap align-top">

		<div class="half">
			<a href="<?php the_field('left_image_link'); ?>">
			<?php
			if ( $image_data ) :
				echo wp_get_attachment_image( $image_data['ID'], 'small', true, array( 'class' => 'fifty-image' ) );
			endif;
			?>
		</a>
			
		</div>

		<div class="half">
		<div class="text">
			<?php echo hyd__get_the_content( $text ); // WPCS: XSS OK. ?>
			</div>
		</div>

	</div>
</section>
