<?php
/**
 * The template used for displaying a Hero block.
 *
 * @package Gourmet Nuts & Dried Fruits
 */

// Set up fields.
$block_title = get_field( 'title' );
$logo       = get_field( 'logo' );

$button_text      = get_field( 'button_text' );
$button_url     = get_field( 'button_url' );
$button_icon       = get_field( 'button_icon' );
if( $button_icon ):
	$url = $button_icon['url'];
    $title = $button_icon['title'];
	$description = $button_icon['description'];
    $alt = $button_icon['alt'];

    // Thumbnail size attributes.
    $size = 'thumbnail';
    $thumb = $button_icon['sizes'][ $size ];
    $width = $button_icon['sizes'][ $size . '-width' ];
    $height = $button_icon['sizes'][ $size . '-height' ];
endif;

$sec_button_text      = get_field( 'sec_button_text' );
$sec_button_url     = get_field( 'sec_button_url' );
$sec_button_icon      = get_field( 'sec_button_icon' );
if( $sec_button_icon ):
	$url = $sec_button_icon['url'];
    $title = $sec_button_icon['title'];
	$description = $sec_button_icon['description'];
    $alt = $sec_button_icon['alt'];

    // Thumbnail size attributes.
    $size = 'thumbnail';
    $thumb = $sec_button_icon['sizes'][ $size ];
    $width = $sec_button_icon['sizes'][ $size . '-width' ];
    $height = $sec_button_icon['sizes'][ $size . '-height' ];
endif;

$third_button_text      = get_field( 'third_button_text' );
$third_button_url     = get_field( 'third_button_url' );
$third_button_icon      = get_field( 'third_button_icon' );
if( $third_button_icon ):
	$url = $third_button_icon['url'];
    $title = $third_button_icon['title'];
	$description = $third_button_icon['description'];
    $alt = $third_button_icon['alt'];

    // Thumbnail size attributes.
    $size = 'thumbnail';
    $thumb = $third_button_icon['sizes'][ $size ];
    $width = $third_button_icon['sizes'][ $size . '-width' ];
    $height = $third_button_icon['sizes'][ $size . '-height' ];
endif;


$alignment   = hyd__get_block_alignment( $block );
$classes     = hyd__get_block_classes( $block );
$secondary   = get_field('secondary_button');

// Start a <container> with possible block options.
hyd__display_block_options(
	array(
		'block'     => $block,
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block hero' . esc_attr( $alignment . $classes ), // Container class.
	)
);
?>
	<div class="wrap">
		<div class="hero-content">
			<!-- LOGO START -->
			<?php if ( $logo ) : ?>
				<img src="<?php echo esc_html( $logo ); ?>" class="hero-logo" alt="logo">
			<?php endif; ?>
			<!-- LOGO END -->

			<!-- CERTIFICATIONS START -->
			<div class="hero-certifications">
				<div class="hero-certifications-wrap">
					<?php if(get_field('certifications')): 
						
						?>
					
						<?php while(has_sub_field('certifications')): 
							$certification_logo      = get_sub_field( 'certification_logo' );
						
							if( $certification_logo ):
								$url = $certification_logo['url'];
								$title = $certification_logo['title'];
								$description = $certification_logo['description'];
								$alt = $certification_logo['alt'];
							
								// Thumbnail size attributes.
								$size = 'thumbnail';
								$thumb = $certification_logo['sizes'][ $size ];
								$width = $certification_logo['sizes'][ $size . '-width' ];
								$height = $certification_logo['sizes'][ $size . '-height' ];
							endif;
							?>

						<a href="<?php the_sub_field('certification_link'); ?>" class="certification" target="_blank" rel="noreferrer"> 
							<img src="<?php echo esc_url( $certification_logo['url'] ); ?>" alt="<?php echo esc_attr($certification_logo['alt']); ?>"  title="<?php echo esc_attr($certification_logo['title']); ?>" description="<?php echo esc_attr($certification_logo['description']); ?>" class="icon" width="<?php echo esc_attr( $certification_logo['sizes'][ $size . '-width' ] ); ?>"  height="<?php echo esc_attr( $certification_logo['sizes'][ $size . '-height' ] ); ?>" class="certification-logo">
						</a>
					<?php 
					endwhile;
					endif; 
					?>
				</div>
			</div>
			<!-- CERTIFICATIONS END -->

			<!-- TITLE START -->
			<?php 
			if ( $block_title ) : 
			hyd__display_hero_heading( $block_title );
			endif;	
			?>
			<!-- TITLE END -->

			<!-- BUTTONS START -->
			<div class="hero-buttons">
				<a class="button button-orange-text button-icon" href="<?php echo $button_url; ?>">
					<img src="<?php echo esc_url( $button_icon['url'] ); ?>" alt="<?php echo esc_attr($button_icon['alt']); ?>"  title="<?php echo esc_attr($button_icon['title']); ?>" description="<?php echo esc_attr($button_icon['description']); ?>" class="icon" width="<?php echo esc_attr( $button_icon['sizes'][ $size . '-width' ] ); ?>"  height="<?php echo esc_attr( $button_icon['sizes'][ $size . '-height' ] ); ?>">
					<?php echo esc_html( $button_text ); ?>
				</a>

				<a class="button button-green-text button-icon" href="<?php echo $sec_button_url; ?>">
					<img src="<?php echo esc_url( $sec_button_icon['url'] ); ?>" alt="<?php echo esc_attr($sec_button_icon['alt']); ?>" title="<?php echo esc_attr($sec_button_icon['title']); ?>" description="<?php echo esc_attr($sec_button_icon['description']); ?>" class="icon"  width="<?php echo esc_attr( $sec_button_icon['sizes'][ $size . '-width' ] ); ?>"  height="<?php echo esc_attr( $sec_button_icon['sizes'][ $size . '-height' ] ); ?>">
					<?php echo esc_html( $sec_button_text); ?>
				</a>

				<a class="button button-bright-yellow-text button-icon" href="<?php echo $third_button_url; ?>">
					<img src="<?php echo esc_url( $third_button_icon['url'] ); ?>" alt="<?php echo esc_attr($third_button_icon['alt']); ?>" title="<?php echo esc_attr($third_button_icon['title']); ?>" description="<?php echo esc_attr($third_button_icon['description']); ?>" class="icon"  width="<?php echo esc_attr( $third_button_icon['sizes'][ $size . '-width' ] ); ?>"  height="<?php echo esc_attr( $third_button_icon['sizes'][ $size . '-height' ] ); ?>">
					<?php echo esc_html( $third_button_text); ?>
				</a>
			</div>
			<!-- BUTTONS END -->
		</div>
	</div>
</section>
