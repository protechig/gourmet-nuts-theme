<?php
/**
 *  The template used for displaying Product Categories.
 *
 * @package Gourmet Nuts & Dried Fruits
 */

// Set up fields.

$category = get_field( 'category_name' );
$button_text       = get_field( 'button_text' );
$button_url       = get_field( 'button_url' );
$icon           = get_field( 'button_icon' );
$icon_hover          = get_field( 'button_icon_hover' );
$alignment   = hyd__get_block_alignment( $block );
$classes     = hyd__get_block_classes( $block );

// Start a <container> with possible block options.
hyd__display_block_options(
    array(
        'block'     => $block,
        'container' => 'section', // Any HTML5 container: section, div, etc...
        'class'     => 'content-block product-categories-block' . esc_attr( $alignment . $classes ), // Container class.
    )
);
?>
    <div class="wrap">
        <div class="header">
            <h3 class="header-title"><?php echo esc_html( $category );  // WPCS: XSS OK. ?></h3>
        </div>
        
        <div class="products display-flex">
        <?php
            // check if the repeater field has rows of data
            if( have_rows('products') ):
                // loop through the rows of data
                while ( have_rows('products') ) : the_row();
                $image      = get_sub_field( 'product_icon' );
                $icon_alt      = get_sub_field( 'product_icon_alt_text' );
                $name       = get_sub_field( 'product_name' );
                $product_category_link   = get_sub_field('product_category_link');
                ?>
                
                <div  class="icon-card fifth">
                    <div class="category-image">
                        <a href="<?php echo esc_url( $product_category_link ); ?>">
                            <img src="<?php echo $image; ?>" alt="<?php echo $icon_alt; ?>">
                        </a>
                    </div>
                    
                    <p class="name"><?php echo  $name  ?> </p>
                </div>
                
                <?php
                endwhile;
            else :
            // no rows found
            endif;
            ?>
        </div>

        <a class="button button-icon button-cat" href="<?php echo  $button_url; ?>">
            <img src="<?php echo $icon; ?>" alt="" class="icon">
            <img src="<?php echo $icon_hover; ?>" alt="" class="icon-hover">
            
            <?php echo esc_html( $button_text); ?>
        </a>
    
	</div>
</section>
