<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gourmet Nuts & Dried Fruits
 */

?>

	<article <?php post_class( 'blog-post' ); ?>>
		
			<?php
			if ( is_single() ) :
				echo '';
			else : 
				echo '<a href="' . get_permalink() . '">';
				the_post_thumbnail();
				echo '</a>';
			endif;
		?>
		<div class="entry-content-container">
			<header class="entry-header container">
				<?php
				if ( is_single() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;
				if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta">
				by <span class="author vcard"><?php the_author(); ?></span><?php
				hyd__posted_on();hyd__entry_footer(); ?>
				</div><!-- .entry-meta -->
				<?php endif; ?>
			</header><!-- .entry-header -->

			<div class="entry-content container">
				<?php
					// Making an excerpt of the blog post content
					$excerpt = strip_tags($post->post_content);
					if (strlen($excerpt) > 100) {
					$excerpt = substr($excerpt, 0, 100);
					$excerpt = substr($excerpt, 0, strrpos($excerpt, ' '));
					$excerpt .= '...';
					}?>
					<p class="excerpt"><?php echo $excerpt ?></p>

					<!-- <a class="read-more" href="<?php get_permalink(); ?>">Read More »</a> -->
					<?php

				?>
			</div><!-- .entry-content -->
		</div>
	</article><!-- #post-## -->
	