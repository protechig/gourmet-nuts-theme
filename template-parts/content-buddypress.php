<?php
/**
 * The template used for displaying page content in buddypress.php
 *
 * @package Gourmet Nuts & Dried Fruits
 */

?>

	<article <?php post_class(); ?>>

		<div class="entry-content">
			<?php the_content(); ?>
		</div><!-- .entry-content -->

	</article><!-- #post-## -->
