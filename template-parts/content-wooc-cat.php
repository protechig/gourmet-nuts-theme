<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gourmet Nuts & Dried Fruits
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( ' wooc-category' ); ?>>
	<?php
		if ( is_product_category() ){
			global $wp_query;

			// get the query object
			$cat = $wp_query->get_queried_object();

			// get the thumbnail id using the queried category term_id
			$thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 

			// get the image URL
			$image = wp_get_attachment_url( $thumbnail_id ); 
			
			// if image is present display hero
			if ( $image ) {
				?>
				<div class="product_cat_hero">
				<style>
				.product_cat_hero::after {
					background-image:radial-gradient(ellipse closest-side, rgba(15, 14, 22, 0.5), #100e17),url(<?php echo $image ?>);
					
					}
				</style>
					<div class="inside_product_cat_hero">
						<h1><?php single_term_title() ?></h1>
					</div>
				</div>
			<?php
			}
		}
		?>
		<div class="container">
		
		<div class="entry-content">
			<?php
				the_content();

				wp_link_pages(
					array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'gns' ),
						'after'  => '</div>',
					)
				);
			?>
		</div><!-- .entry-content -->

		<?php if ( get_edit_post_link() ) : ?>
			<footer class="entry-footer">
				<?php
					edit_post_link(
						sprintf(
							/* translators: %s: Name of current post */
							esc_html__( 'Edit %s', 'gns' ),
							the_title( '<span class="screen-reader-text">"', '"</span>', false )
						),
						'<span class="edit-link">',
						'</span>'
					);
				?>
			</footer><!-- .entry-footer -->
		<?php endif; ?>
</div>
	</article><!-- #post-## -->
