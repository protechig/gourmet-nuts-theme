<?php
/**
 * Customizer sections.
 *
 * @package Gourmet Nuts & Dried Fruits
 */

/**
 * Register the section sections.
 *
 * @author WDS
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function hyd__customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'hyd__additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'gns' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'hyd__social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'gns' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'gns' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'hyd__header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'gns' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'hyd__footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'gns' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'hyd__customize_sections' );
