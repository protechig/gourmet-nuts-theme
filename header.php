<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gourmet Nuts & Dried Fruits
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin-top: 0 !important;">
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="alternate" href="<?php esc_url(the_permalink())?>" hreflang="en-us">
	<link rel="alternate" href="<?php esc_url(the_permalink())?>" hreflang="x-default">

	<?php wp_head(); ?>
</head>

<body <?php body_class( 'site-wrapper' ); ?>>
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'gns' ); ?></a>

	<!-- HEADER START -->
	<header class="site-header  background-gallery" id="header">
		<div class="nav-bar wide-container">
			<!-- LOGO START -->
			<div class="site-logo">
				<div class="site-branding">
					<?php the_custom_logo(); ?>
				</div><!-- .site-branding -->
			</div><!-- .logo -->
			<!-- LOGO END -->

			<!-- NAV START -->
			<div class="gns-navigation">
				<nav id="site-navigation" class="main-navigation" aria-label="<?php esc_attr_e( 'Main Navigation', 'gns' ); ?>">
					<div class="header-main container">

						<!-- PRIMARY NAV START -->
						<div class="header-main__primary">
							<?php
							wp_nav_menu(
								array(
									'fallback_cb'    => false,
									'theme_location' => 'primary',
									'menu_id'        => 'primary-menu',
									'menu_class'     => 'menu dropdown container',
									'container'      => false,
								)
							);
							?>
						</div>
						<!-- PRIMARY NAV END -->

						<!-- CATAGORY NAV START -->
						<div class="header-main__secondary">
							<?php

							wp_nav_menu(
								array(
									'fallback_cb'    => false,
									'theme_location' => 'category-menu',
									'menu_id'        => 'category-menu',
									'menu_class'     => 'menu dropdown container',
									'container'      => false,
								)
							);
							
							?>
						</div>
						<!-- CATEGORY NAV END -->
					</div> <!-- .header-main .container -->
				</nav> <!-- #site-navigation-->
			</div> <!-- .gns-navigation -->
			<!-- NAV END -->
		</div><!-- .nav-bar .wide-container -->
	</header><!-- .site-header-->
	<!-- HEADERR END -->