<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 40);
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_template_product_description', 40 );

function woocommerce_template_product_description() {
	wc_get_template( 'single-product/tabs/description.php' );
}

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
?>

<!-- CONTENT START -->
<main id="main" class="site-main">
	<div class="wrap">
		<?php

		do_action( 'woocommerce_before_single_product' );

		if ( post_password_required() ) {
			echo get_the_password_form(); // WPCS: XSS ok (mira- not my comment idk what this means)
			return;
		}

		?>

		<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
			<?php

			/**
			 * Hook: woocommerce_before_single_product_summary.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );

			?>

			<div class="summary entry-summary">
				<?php

				/**
				 * Hook: woocommerce_single_product_summary.
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 * @hooked WC_Structured_Data::generate_product_data() - 60
				 */
				do_action( 'woocommerce_single_product_summary' );
				
				?>
			</div>
			<!-- PRODUCT SUMMARY END -->
		</div>
	</div>
		
	<!-- BANNAR START -->
	<section class="promotion-section">
		<style>
			.promotion-section:after {
				background-image: url(<?php the_field('background_image', 'option'); ?>)
			}
		</style>

		<div class="container">
			<h2 class="center" style="margin-bottom: 0;"><?php the_field('promotion_text', 'option'); ?></h2>
		</div>
	</section>
	<!-- BANNAR END -->

	<!-- PRODUCT DESCRIPTION START -->
	<div class="product-description wrap">
		<?php

		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
		
		?>

		<?php
			// if(get_field('youtube_video')) : 

			// $video = get_field( 'youtube_video' );

			// preg_match('/src="(.+?)"/', $video, $matches_url );
			// $src = $matches_url[1];

			// preg_match('/embed(.*?)?feature/', $src, $matches_id );
			// $id = $matches_id[1];
			// $id = str_replace( str_split( '?/' ), '', $id );
		?>
			<!-- <div class="embed-container">
				<iframe
				  width="560"
				  height="315"
				  src="<?= $src ?>&autoplay=1"
				  srcdoc="<style>*{padding:0;margin:0;overflow:hidden}html,body{height:100%}img,span{position:absolute;width:100%;top:0;bottom:0;margin:auto}span{height:1.5em;text-align:center;font:48px/1.5 sans-serif;color:white;text-shadow:0 0 0.5em black}</style><a href=<?= $src ?>&autoplay=1><img src=http://img.youtube.com/vi/<?= $id; ?>/mqdefault.jpg alt='Product Video'><span>▶</span></a>"
				  frameborder="0"
				  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
				  allowfullscreen
				  title=""
				></iframe>
			</div> -->
		<?php //endif; ?>
	</div>
	<!-- PRODUCT DESCRIPTION END -->

	<?php do_action( 'woocommerce_after_single_product' ); ?>
</main>
<!-- CONTENT END -->