<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Gourmet Nuts & Dried Fruits
 */

get_header(); ?>

	<main id="main" class="site-main">
	<div class="full-width">
			<div class="error-404 not-found container container">
				<div class="grid-wrapper">
					<header class="page-header">
						<h1 class="page-title"><?php esc_html_e( 'Sorry, this page doesn\'t exist.', 'gns' ); ?></h1>
					</header><!-- .page-header -->

			<div class="page-content">

				<p><?php esc_html_e( 'It seems we can\'t find what you\'re looking for. Perhaps searching can help.', 'gns' ); ?></p>

				<?php //get_search_form(); ?>

			</div><!-- .page-content -->
			</div>
			</div>
</div>

	</main><!-- #main -->

<?php get_footer(); ?>
