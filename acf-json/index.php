<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gourmet Nuts & Dried Fruits
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
