<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Gourmet Nuts & Dried Fruits
 */

get_header(); ?>
<main id="main" class="site-main">
	<div class="full-width">
	<div class="container">
		<div class="display-flex grid-wrapper wrap">
				<div class="container">
				<?php
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', 'single' );


					// the_post_navigation();

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
			</div>
		</div><!-- .grid-wrapper -->
	</div>
	</div>
	</main><!-- #main -->
<?php get_footer(); ?>
